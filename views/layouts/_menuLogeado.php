<?php

use yii\bootstrap5\Nav;
use yii\bootstrap5\Html;

echo Nav::widget([
    'options' => ['class' => 'navbar-nav'],
    'items' => [
        ['label' => 'Home', 'url' => ['/site/index']],
        ['label' => 'Ejercicio 1', 'url' => ['/site/ejercicio1']],
        ['label' => 'Ejercicio 2', 'url' => ['/site/ejercicio2']],
        ['label' => 'Tabla clientes', 'url' => ['/cliente/index']],
        '<li class="nav-item">'
            . Html::beginForm(['/site/logout'])
            . Html::submitButton(
                'Cerrar sesión (' . Yii::$app->user->identity->username . ')',
                ['class' => 'nav-link btn btn-link logout']
            )
            . Html::endForm()
            . '</li>'
    ]
]);
