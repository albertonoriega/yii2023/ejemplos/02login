<?php

/** @var yii\web\View $this */

$this->title = 'My Yii Application';
?>
<div class="site-index">

    <div class="jumbotron text-center bg-transparent mt-5 mb-5">
        <h1 class="display-4">Ejemplo de logeos</h1>
    </div>

    <div class="body-content">

        <div class="row">
            <div class="col-lg-4 mb-3">
                <p>Hemos creado 3 vistas, ejercicio1, ejercicio2, ejercicio3</p>
                <p>Para poder acceder a las vistas ejercicio1 y ejercicio2 tenemos que estar logeados</p>
                <p>Para poder acceder a la vista ejercicio3 tenemos que ser invitados obligatoriamente, si estamos logeados no funcionará.</p>
            </div>
            <div class="col-lg-4 mb-3">
                <p>Para gestionar que vista puede ver cada usuario, lo realizamos en el SiteController en el método behaviours</p>
                <p>En only ponemos las vistas que vamos a controlar con el logeo</p>
                <p>@ indica que el usuario está logeado</p>
                <p>? indica que el usuario no está logeado(Invitado) </p>
            </div>
            <div class="col-lg-4">
                <p>Hemos cambiado el menu y hemos creado dos subvistas</p>
                <p>La primera _menuInvitado, carga el menu con las vistas que puede ver el usuario sin estar logeado</p>
                <p>La segunda _menuLogeado, carga el menu con las vistas que puede ver el usuario cuando está logeado</p>
            </div>
        </div>

        <div class="row mt-5">
            <p>Se crea un modelo y un CRUD de la tabla de cliente de la BBDD para gestionar los clientes</p>
            <p>En el controlador de clientes, en el método behaviours, hacemos que si el usuario está logeado pueda acceder a la tabla y gestionarla (index, create, update, view). Si no está logeado, no podrá acceder a nada.</p>
        </div>

        <div style="border: 3px solid red; padding:5px">
            <div class="row mt-1">
                <h3 style="color: red;">Ejemplo 02Login</h3>
            </div>
            <div class="row mt-1">
                <p>Hemos creado un GeneralController que hereda de Controller. Este controlador es el que va a gestionar todos los behaviours</p>
                <p>Tanto ClienteController como Sitecontroller, heredan de GeneralController</p>
            </div>
        </div>

    </div>
</div>