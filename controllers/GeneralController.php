<?php

namespace app\controllers;

use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\web\Controller;

class GeneralController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                // acciones del controlador que voy a gestionar 
                'only' => ['*'],
                'rules' => [
                    [
                        'actions' => [], // los usuarios pueden realizar todas las acciones
                        'controllers' => ['site', 'cliente'],
                        'allow' => true,
                        'roles' => ['@'], // @ indica que el usuario está logeado  
                    ],
                    [
                        'actions' => ['index', 'login', 'error', 'ejercicio3'],
                        'controllers' => ['site'],
                        'allow' => true,
                        'roles' => ['?'], // ? indica que el usuario no está logeado
                    ],
                    [
                        'actions' => [''], // En el controlador cliente, los usuarios wue no están logeados no pueden hacer nada
                        'controllers' => ['cliente'],
                        'allow' => true,
                        'roles' => ['?'], // ? indica que el usuario no está logeado
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }
}
