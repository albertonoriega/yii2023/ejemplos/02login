DROP DATABASE IF EXISTS clientesLogin;

CREATE DATABASE clientesLogin;
USE clientesLogin;

CREATE TABLE cliente(
idCliente int,
nombreCliente varchar(100),
apellidosCliente varchar(100),
correoCliente varchar(100),
fechaNacimiento date,
PRIMARY KEY (idCliente)
);